<html>
    <head>
        <title>Google Calender API</title>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="script.js"></script>

    </head>

    <body>
        
        <h1>INVOICE GENERATOR</h1>

        <form action="getEvents.php" method="post">
            
            <table>
                <tr>
                    <td>Start Date:</td>
                    <td><input type="text" name="startDate" class="datepicker" required></td>
                </tr>
                <tr>
                    <td>End Date:</td>
                    <td><input type="text" name="endDate" class="datepicker" required></td>
                </tr>
                 <tr>
                    <td>Invoice Name:</td>
                    <td><input type="text" name="invName" required></td>
                </tr>
                 <tr>
                    <td>Rate:</td>
                    <td><input type="text" name="rate" required></td>
                </tr>
                <tr></tr>
                <tr></tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Generate"></td>
                    
                    
                </tr>
            </table>
            
            <br><br>
          

        </form>

    </body>

</html>