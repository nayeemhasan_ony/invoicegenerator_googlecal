<?php

class report {

    public $fileName;
    public $rate;
    public $grandTotal;
    

    function generateFile($events) {
        
        $mcgcADM = [];$mcgcSYD = [];$mcgcBRIS = []; $mcgcSHZ = [];$mcgcMEL = [];$chrofi = [];
        
        $timeS = [$mcgcADM,$mcgcBRIS,$mcgcSHZ,$mcgcMEL,$chrofi];
        

        $fileName = $this->fileName;
        $rate = $this->rate;
        $grandTotal = 0;
         
        //echo $fileName;
        if (file_exists($fileName)) {
            @chmod( $fileName, 0777 );
                @unlink( $fileName );
        }

        $headers = array('CLIENT', 'HOURS','RATE','TOTAL');
        //print_r($headers);
        @chmod( $fileName, 0777 );
        $fd = fopen($fileName, 'w+');
        fputcsv($fd, $headers);
        
        //var_dump($events);
        //die();
        
        foreach($events as $event) {
                                  
            
        if (strpos($event['summary'], 'ADM') !== false){
               
                array_push($mcgcADM, $event);
       
    }
    
     if (strpos($event['summary'], 'SYD') !== false){
               
                array_push($mcgcSYD, $event);
       
    }
    
          
        if (strpos($event['summary'], 'BRI') !== false){
               
                array_push($mcgcBRIS, $event);
       
    }
    
          
        if (strpos($event['summary'], 'SHZ') !== false){
               
                array_push($mcgcSHZ, $event);
       
    }
    
          
        if (strpos($event['summary'], 'MEL') !== false){
               
                array_push($mcgcMEL, $event);
       
    }
    
     if (strpos($event['summary'], 'CHROFI') !== false){
               
                array_push($chrofi, $event);
       
    }
    
    

    
    
        }
        
        $timeS = [$mcgcADM,$mcgcSYD,$mcgcBRIS,$mcgcSHZ,$mcgcMEL,$chrofi];

        
 
    foreach($timeS as $client){
        if(sizeof($client)>0)
        {
            $clientName = $client[0]['summary'];
            $clientName = str_replace("TIMESHEETS", "", $clientName);
            $clientName = str_replace("NH","",$clientName);
            $clientName = trim(preg_replace("/[^A-Z ]/",'', $clientName));
            
            $reportedHour = 0;
            
            
            $total = 0;
            foreach ($client as $eachEvent) {


            $start = $eachEvent->start->dateTime;
            $start = new DateTime($start);

            $end = $eachEvent->end->dateTime;
            $end = new DateTime($end);

            $timeDiff = $start->diff($end);


            $hours = $timeDiff->format("%h");

            $minutes = $timeDiff->format("%i");

            if ($minutes > 0) {
                $hourAdd = $minutes / 60;
                $hours = $hours + $hourAdd;
            }

            $total = $total + ($rate * $hours); 
            $reportedHour = $reportedHour + $hours;
            
            
                                 
            
        }

        $row = array();
            $row['client'] = $clientName;
            $row['hours'] = $reportedHour;
            $row['rate'] = $rate;
            $row['total'] = $total;
            
            
            $grandTotal = $grandTotal + $total;
            
            fputcsv($fd,$row);
        }
    }




    
    //final row
        $finalRow = array();
        $finalRow['client']='';
        $finalRow['hours']='';
        $finalRow['rate'] = 'Total to Pay';
        $finalRow['total'] = $grandTotal;
        
        fputcsv($fd, $finalRow);
        echo "Please check report";
    
    }
    

}

?>